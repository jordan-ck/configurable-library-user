defmodule LibraryUser.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do

    IO.puts "Starting up. Configurable Library values:"
    IO.puts "ConfigurableLibrary.value1(): #{ConfigurableLibrary.value1()}"
    IO.puts "ConfigurableLibrary.value2(): #{ConfigurableLibrary.value2()}"
    IO.puts "ConfigurableLibrary.value3() (overridden in config/config.exs): #{ConfigurableLibrary.value3()}"
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: LibraryUser.Worker.start_link(arg)
      # {LibraryUser.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: LibraryUser.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
