defmodule LibraryUser do
  @moduledoc """
  Documentation for LibraryUser.
  """

  @doc """
  Hello world.

  ## Examples

      iex> LibraryUser.hello
      :world

  """
  def hello do
    :world
  end
end
